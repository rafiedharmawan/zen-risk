"""zenrisk URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from django.views.generic.base import RedirectView
import data_management.urls as data_management
import onboarding.urls as onboarding
import classical_model.urls as classical_model
import challenger_model.urls as challenger_model
import rules_definition.urls as rules_definition 

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='/onboarding',permanent=True), name = 'index')
    path('admin/', admin.site.urls),
    path('data-management/', include(data_management,namespace='data_management')),
    path('onboarding/', include(onboarding,namespace='onboarding')),
    path('classical_model/', include(classical_model,namespace='classical_model')),
    path('challenger_model/', include(challenger_model,namespace='challenger_model')),
    path('rules_definition/', include(rules_definition,namespace='rules_definition')),

]
