from django.db import models

class DataManagement(models.Model):
    namaVariable = models.CharField(max_length=250)
    numberOfObservations = models.CharField(max_length=10)
    numberOfMissingObv = models.CharField(max_length=10)
    mode = models.CharField(max_length=10)
    minimumValue = models.CharField(max_length=10)
    maximumValue = models.CharField(max_length=10)
    standardDeviation = models.CharField(max_length=10)

    def __str__(self):
        return self.message
