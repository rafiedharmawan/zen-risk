from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import View
from .models import DataManagement

class DataView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'chart.html', {})

def get_data(request, *args, **kwargs):
    labels = ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange']
    default_items = [1,2,3,4,5,6]
    data = {
        "labels" : labels,
        "default" : default_items,
        "trainSensitivity" : 100,
        "trainSpecitivity" : 100,
        "valSensitivity" : 100,
        "valSpecitivity" : 100,
    }
    return JsonResponse(data)

def get_data2(request, *args, **kwargs):
    data = {
        "sumpComp1" : 10000,
        "sumpComp2" : 10000,
        "sumpComp3" : 10000,
        "sumpComp4" : 10000,
        "sumpComp5" : 10000,
    }
    return JsonResponse(data)