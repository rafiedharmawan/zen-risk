from django.conf.urls import url
from django.urls import path
from .views import index
#url for app
app_name = 'data_management'
urlpatterns = [
    path('', index, name='index'),
]

