from django.apps import AppConfig


class ChallengerModelConfig(AppConfig):
    name = 'challenger_model'
