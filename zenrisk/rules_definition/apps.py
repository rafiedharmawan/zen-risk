from django.apps import AppConfig


class RulesDefinitionConfig(AppConfig):
    name = 'rules_definition'
