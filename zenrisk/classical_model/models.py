from django.db import models

class ClassicModel(models.Model):
    trainSensitivity = models.IntegerField()
    trainSpecificity = models.IntegerField()
    valSensitivity = models.IntegerField()
    valSpecificity = models.IntegerField()
    sumComp1 = models.IntegerField()
    sumComp2 = models.IntegerField()
    sumComp3 = models.IntegerField()
    sumComp4 = models.IntegerField()
    sumComp5 = models.IntegerField()

    def __str__(self):
        return self.message
