from django.apps import AppConfig


class ClassicalModelConfig(AppConfig):
    name = 'classical_model'
