from django.apps import AppConfig


class ZenappsConfig(AppConfig):
    name = 'zenapps'
